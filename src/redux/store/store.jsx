import { configureStore } from "@reduxjs/toolkit";
import products from "../reducers/products";
import modal from "../reducers/modal";

export const store = configureStore({
  reducer: {
    products,
    modal,
  },
});
