import React, { useEffect } from "react";
import { Layout } from "./layout";
import { useDispatch } from "react-redux";
import { getData } from "./redux/extraReducers/getData";
import { setFavorites, setProductsInCart } from "./redux/reducers/products";

// load Favorites from localStorage
const favoriteIdsFromStorage = localStorage.getItem("favorites");
// load products in Cart from localStorage
const cartIdsFromStorage = localStorage.getItem("Cart");

export const App = () => {
  const dispatch = useDispatch();

  /*--------------------LOAD PRODUCTS--------------------*/
  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  /*--------------------MOUNT COMPONENT--------------------*/
  useEffect(() => {
    if (favoriteIdsFromStorage) {
      dispatch(setFavorites(JSON.parse(favoriteIdsFromStorage)));
    }
    if (cartIdsFromStorage) {
      dispatch(setProductsInCart(JSON.parse(cartIdsFromStorage)));
    }
  }, [dispatch]);

  /*--------------------RENDER--------------------*/
  return <Layout />;
};
